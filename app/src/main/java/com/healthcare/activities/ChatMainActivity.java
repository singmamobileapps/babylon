package com.healthcare.activities;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.healthcare.R;

import java.util.Locale;

public class ChatMainActivity extends AppCompatActivity {

    TextToSpeech textToSpeech;
    int result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_main2);

        textToSpeech =new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    result = textToSpeech.setLanguage(new Locale("EN"));
                    textToSpeech.setPitch(0.85f);
                    textToSpeech.setSpeechRate(0.80f);
                    textToSpeech.speak("Tell me whats troubling to you ",TextToSpeech.QUEUE_FLUSH,null);
//                    Log.e("locale value", "heloo  dear"+context.getResources().getConfiguration().locale );
//                    textToSpeech.setLanguage( context.getResources().getConfiguration().locale);

//                    if (status==TextToSpeech.SUCCESS ){
//
//                         result=textToSpeech.setLanguage(new Locale("de_NL"));
//
//                    }

//                    result = textToSpeech.setLanguage(Locale.FRENCH);
//                    result = textToSpeech.setLanguage(Locale.GERMAN);
//                    result = textToSpeech.setLanguage(Locale.ITALIAN);

                } else {

                    Toast.makeText(getApplicationContext(), "featured not supported in your device", Toast.LENGTH_SHORT).show();

                }
            }
        });
        ImageView send = findViewById(R.id.send);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),ChatBotActivity.class));
                finish();
            }
        });
    }
}
