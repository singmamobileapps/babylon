package com.healthcare.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import com.healthcare.R;
import com.healthcare.fragments.HomeFragment;
import com.healthcare.utils.IntentAndFragmentService;

public class DashBoard extends AppCompatActivity {

    FrameLayout frame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        frame = findViewById(R.id.frame);
        findViewById(R.id.chat_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashBoard.this,ChatMainActivity.class));
            }
        });
        IntentAndFragmentService.fragmentdisplay(DashBoard.this,R.id.frame,new HomeFragment(),null,false,false);
    }
}
