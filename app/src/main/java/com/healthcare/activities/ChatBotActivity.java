package com.healthcare.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.healthcare.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class ChatBotActivity extends AppCompatActivity {


    String[] questions = {"Choose the one which applies to you", "is it accompanied by some sort of bleeding " , "Is there any swallen area ? " };

    HashMap<String,ArrayList> ans = new HashMap<>();

    TextToSpeech textToSpeech;
    int result;
    int questionIndex=0;
    ScrollView scrollView;
    LinearLayout baselinearlayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayList<String> ans1 = new ArrayList<>();
        ans1.add("Headache causes because of impact");
        ans1.add("Intense headache and sensitive to light");


        ArrayList<String> ans2 = new ArrayList<>();
        ans2.add("Yes");
        ans2.add("No");
        ans2.add("Not Sure");

        ArrayList<String> ans3 = new ArrayList<>();
        ans3.add("Small bump");
        ans3.add("Big bump");
        ans3.add("Not Sure");

        ans.put(questions[0],ans1);
        ans.put(questions[1],ans2);
        ans.put(questions[2],ans3);

        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    result = textToSpeech.setLanguage(new Locale("EN"));
                    textToSpeech.setPitch(0.85f);
                    textToSpeech.setSpeechRate(0.80f);
                    textToSpeech.speak("Choose the one which applies to you ", TextToSpeech.QUEUE_FLUSH, null);
//                    Log.e("locale value", "heloo  dear"+context.getResources().getConfiguration().locale );
//                    textToSpeech.setLanguage( context.getResources().getConfiguration().locale);

//                    if (status==TextToSpeech.SUCCESS ){
//
//                         result=textToSpeech.setLanguage(new Locale("de_NL"));
//
//                    }

//                    result = textToSpeech.setLanguage(Locale.FRENCH);
//                    result = textToSpeech.setLanguage(Locale.GERMAN);
//                    result = textToSpeech.setLanguage(Locale.ITALIAN);

                } else {

                    Toast.makeText(getApplicationContext(), "featured not supported in your device", Toast.LENGTH_SHORT).show();

                }
            }
        });
        setContentView(R.layout.activity_chat_bot2);

         scrollView = findViewById(R.id.scroll);


         baselinearlayout = new LinearLayout(this);

        //Setting up LinearLayout Orientation
        baselinearlayout.setOrientation(LinearLayout.VERTICAL);

        // LinearLayout.LayoutParams linearlayoutlayoutparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);


        baselinearlayout.addView(getBabylonQuestion());


        final CardView[] cards = getBabylonAnswers();

        handleAnswers(cards);
        scrollView.addView(baselinearlayout);
    }

    private void handleAnswers(final CardView[] cards) {


        for(int i=0;i<cards.length;i++) {
            baselinearlayout.addView(cards[i]);
            cards[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    for(int i=0;i<cards.length;i++) {

                        if(view !=cards[i] )
                            cards[i].setVisibility(View.GONE);


                    }

                    questionIndex++;
                    baselinearlayout.addView(getBabylonQuestion());

                    textToSpeech.speak(questions[questionIndex], TextToSpeech.QUEUE_FLUSH, null);

                    handleAnswers(getBabylonAnswers());

                }
            });
        }


    }


    LinearLayout getBabylonQuestion(){

        //Creating LinearLayout.
        LinearLayout linearlayout = new LinearLayout(this);

        linearlayout.setPadding(10,50,50,70);
        //Setting up LinearLayout Orientation
        linearlayout.setOrientation(LinearLayout.HORIZONTAL);

        LinearLayout.LayoutParams linearlayoutlayoutparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);



        LinearLayout.LayoutParams LayoutParamsview = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        //Creating textview .
        ImageView imageView = new ImageView(this);

        imageView.setImageResource(R.drawable.chat_icon);
        imageView.setPadding(30,0,20,0);
        TextView SampleTextView2 = new TextView(this);

        //Adding text to TextView.

        SampleTextView2.setText(questions[questionIndex]);

        SampleTextView2.setPadding(40,0,40,10);
        SampleTextView2.setBackgroundColor(Color.parseColor("#FFC6D6C3"));
        //Setting TextView text Size
        SampleTextView2.setGravity(Gravity.CENTER);
        SampleTextView2.setTextSize(25);

        imageView.setLayoutParams(LayoutParamsview);
        SampleTextView2.setLayoutParams(LayoutParamsview);
        SampleTextView2.setBackground(getDrawable(R.drawable.pinkbg));

        //Adding textview to linear layout using Add View function.
        linearlayout.addView(imageView);
        linearlayout.addView(SampleTextView2);

        return linearlayout;

    }

    CardView[] getBabylonAnswers(){



        ArrayList<String> answers = ans.get(questions[ questionIndex]);
        CardView[] cards = new CardView[answers.size()];
        for(int i=0;i<answers.size();i++) {

            CardView card = new CardView(this);

            // Set the CardView layoutParams
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            LinearLayout.LayoutParams cardParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            cardParams.setMargins(335, 15, 35, 25);
            card.setLayoutParams(cardParams);


            // Set CardView corner radius
            card.setRadius(9);

            // Set cardView content padding
            card.setContentPadding(15, 15, 15, 15);

            // Set a background color for CardView
            //card.setCardBackgroundColor(Color.parseColor("#FFC6D6C3"));

            // Set the CardView maximum elevation
            card.setMaxCardElevation(15);

            // Set CardView elevation
            card.setCardElevation(9);

            // Initialize a new TextView to put in CardView
            TextView tv = new TextView(this);
            tv.setLayoutParams(params);

            tv.setGravity(Gravity.CENTER);
            tv.setText(answers.get(i));
            tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 22);
            // tv.setTextColor(Color.RED);

            // Put the TextView in CardView
            card.addView(tv);

            cards[i]= card;
        }



        return  cards;

    }
}
