package com.healthcare.interfaces;

import android.view.View;

public interface CustomItemClickListener {
    public void onItemClick(View v, int position, String tag);
}
