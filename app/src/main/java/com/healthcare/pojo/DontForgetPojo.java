package com.healthcare.pojo;

public class DontForgetPojo {

    private String when;
    private String tablet;
    private String time;
    private String after_before;

    public DontForgetPojo(String when, String tablet, String time, String after_before) {
        this.when = when;
        this.tablet = tablet;
        this.time = time;
        this.after_before = after_before;
    }


    public String getWhen() {
        return when;
    }

    public void setWhen(String when) {
        this.when = when;
    }

    public String getTablet() {
        return tablet;
    }

    public void setTablet(String tablet) {
        this.tablet = tablet;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAfter_before() {
        return after_before;
    }

    public void setAfter_before(String after_before) {
        this.after_before = after_before;
    }
}

