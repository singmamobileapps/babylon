package com.healthcare.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.healthcare.R;
import com.healthcare.adapter.DontForgetAdapter;
import com.healthcare.interfaces.CustomItemClickListener;
import com.healthcare.pojo.DontForgetPojo;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    RecyclerView dont_forget_rv;
    ArrayList<DontForgetPojo> dontForgetPojos = new ArrayList<>();
    DontForgetPojo dontForgetPojo;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.landing_page,container,false);
        init(view);
        return view;
    }

    public void init(View view)
    {
        dont_forget_rv = view.findViewById(R.id.dont_forget_rv);
        dont_forget_rv.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));

        dontForgetPojo = new DontForgetPojo("Today","Calcium Tablet","3 Pm","After Breakfast");
        dontForgetPojos.add(dontForgetPojo);
        dontForgetPojo = new DontForgetPojo("Tonight","Calcium Tablet","3 Pm","After Breakfast");
        dontForgetPojos.add(dontForgetPojo);
        dontForgetPojo = new DontForgetPojo("Tomorrow","Calcium Tablet","3 Pm","After Breakfast");
        dontForgetPojos.add(dontForgetPojo);


        DontForgetAdapter forgetAdapter = new DontForgetAdapter(getActivity(), dontForgetPojos, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position, String tag) {

            }
        });
        dont_forget_rv.setAdapter(forgetAdapter);

    }


}
