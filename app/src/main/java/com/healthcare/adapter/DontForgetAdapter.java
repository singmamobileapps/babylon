package com.healthcare.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.healthcare.R;
import com.healthcare.interfaces.CustomItemClickListener;
import com.healthcare.pojo.DontForgetPojo;

import java.util.ArrayList;

public class DontForgetAdapter extends RecyclerView.Adapter<DontForgetAdapter.DontForgetVH>
{

    Context context;
    ArrayList<DontForgetPojo> dontForgetPojos;
    CustomItemClickListener itemClickListener;

    public DontForgetAdapter(Context context, ArrayList<DontForgetPojo> dontForgetPojos, CustomItemClickListener itemClickListener) {
        this.context = context;
        this.dontForgetPojos = dontForgetPojos;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public DontForgetVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.today_tonight_card_item,parent,false);
        DontForgetVH forgetVH = new DontForgetVH(view);
        return forgetVH;
    }

    @Override
    public void onBindViewHolder(@NonNull DontForgetVH holder, int position) {

        holder.time.setText(dontForgetPojos.get(position).getTime());
        holder.when.setText(dontForgetPojos.get(position).getWhen());
        holder.tablet.setText(dontForgetPojos.get(position).getTablet());
        holder.after_before.setText(dontForgetPojos.get(position).getAfter_before());

    }

    @Override
    public int getItemCount() {
        return dontForgetPojos.size();
    }

    public class DontForgetVH extends RecyclerView.ViewHolder{

        TextView when,tablet,time,after_before;

        public DontForgetVH(View itemView) {
            super(itemView);

            when=itemView.findViewById(R.id.when);
            tablet=itemView.findViewById(R.id.tablet);
            time=itemView.findViewById(R.id.time);
            after_before=itemView.findViewById(R.id.after_before);

        }
    }

}
